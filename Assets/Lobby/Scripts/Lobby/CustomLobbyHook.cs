﻿using Prototype.NetworkLobby;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CustomLobbyHook : LobbyHook {


    public override void OnLobbyServerSceneLoadedForPlayer(NetworkManager manager, GameObject lobbyPlayer, GameObject gamePlayer)
    {
        base.OnLobbyServerSceneLoadedForPlayer(manager, lobbyPlayer, gamePlayer);

        gamePlayer.GetComponent<Player>().playerName = lobbyPlayer.GetComponent<LobbyPlayer>().playerName;
        gamePlayer.GetComponent<Player>().Color = lobbyPlayer.GetComponent<LobbyPlayer>().playerColor;
    }
}
