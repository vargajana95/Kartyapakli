﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour {


	public void MoveCameraToTable(Player player)
    {
        var players = GameObject.FindGameObjectsWithTag("Player");

        foreach (var p in players)
        {
            Player playerToMove = p.GetComponent<Player>();
            if (!playerToMove.isLocalPlayer)
                playerToMove.MoveTableToBack();
        }

        player.MoveTableToFront();

        transform.DOMove(new Vector3(0, 10, -10), 0.5f);


    }

    public void MoveToCenter()
    {
        transform.DOMove(new Vector3(0, 0, -10), 0.5f);
    }

    public void MoveTownTable()
    {
        transform.DOMove(new Vector3(0, -10, -10), 0.5f);
    }
}
