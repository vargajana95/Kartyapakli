﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour {
    private GameObject parent;

    private int siblingIndex;

    private bool isFaceingUp;


    private CardView cardView;

    // Use this for initialization
    void Start () {
        cardView = GetComponent<CardView>();
	}

    public CardMemento CreateMemento()
    {
        return new CardMemento(parent, siblingIndex, isFaceingUp);
    }

    public void RestoreFromMemento(CardMemento memento)
    {
        parent = memento.Parent;
        siblingIndex = memento.SiblingIndex;
        isFaceingUp = memento.IsFaceingUp;

        //cardView.RpcRestoreState(parent, siblingIndex, isFaceingUp);
    }
	

    public class CardMemento
    {

        public GameObject Parent { get; private set; }

        public int SiblingIndex { get; private set; }


        public bool IsFaceingUp { get; private set; }

        
        public CardMemento(GameObject parent, int siblingIndex, bool isFaceingUp)
        {
            Parent = parent;
            SiblingIndex = siblingIndex;
            IsFaceingUp = isFaceingUp;
        }


    }
}
