﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

public class CardView : DraggableEntity
{
    private Vector3 pointerOffset;

    [SyncVar]
    public string cardSpritePath;

    [SyncVar]
    public string backSpritePath;

    [SyncVar]
    public int cardSpriteIndex;

    [SyncVar(hook = "OnSiblingIndexChange")]
    public int siblingIndex;

    [SyncVar(hook = "OnSortingOrderChanged")]
    public int sortingOrder;



    public bool colliderEnabled = true;

    public bool isFaceingUp = false;

    private bool isLocallyFacingUp = false;

    private Sprite sprite;
    private Sprite backSprite;

    public bool ExIsFacingUp { get; set; }


    protected override void Start()
    {
        base.Start();

        //sprite = Resources.LoadAll<Sprite>("Cards2/cards2")[cardSpriteName];
        sprite = Resources.LoadAll<Sprite>(cardSpritePath)[cardSpriteIndex];
        backSprite = Resources.Load<Sprite>(backSpritePath);


        if (isFaceingUp || isLocallyFacingUp)
            gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
        else
            gameObject.GetComponent<SpriteRenderer>().sprite = backSprite;

        GetComponent<BoxCollider2D>().enabled = colliderEnabled;
        GetComponent<SpriteRenderer>().sortingOrder = sortingOrder;

        ExSiblingIndex = transform.GetSiblingIndex();
        ExIsFacingUp = isFaceingUp;
    }

    

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);

        pointerOffset = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        pointerOffset.z = 0;
    }

    public override void OnDrag(PointerEventData eventData)
    {
        base.OnDrag(eventData);

        //TODO move this up to base
        var screenMousePos = Input.mousePosition;
        screenMousePos.z = 10.0f;
        transform.position = Camera.main.ScreenToWorldPoint(screenMousePos) - pointerOffset;
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);

        pointerOffset = Vector3.zero;
    }

    public override void OnDrop()
    {
        
    }

    public override void OnTouch()
    {
        var parentZone = this.transform.parent.GetComponent<Zone>();
        if (parentZone != null)
            parentZone.OnTouch();
        
        //ModalPanel.Instance().Choice("Do you want to flip the card?", () => SetFacingUp(!isFaceingUp));
        //SetFacingUp(!isFaceingUp);
    }

    public override void OnLongTouch()
    {
        var parentZone = this.transform.parent.GetComponent<Zone>();
        if (parentZone != null)
            parentZone.OnLongTouch();
    }

    /*public void SetFacingUp(bool b)
    {
        isFaceingUp = b;
    }*/

    public void SetFacingUp(bool b)
    {
        isFaceingUp = b;
        if (isFaceingUp || isLocallyFacingUp)
            gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
        else
            gameObject.GetComponent<SpriteRenderer>().sprite = backSprite;
    }

    private void OnSiblingIndexChange(int siblingIndex)
    {
        this.transform.SetSiblingIndex(siblingIndex);
        ExSiblingIndex = transform.GetSiblingIndex();
    }

    public void SetLocallyFacingUp(bool b)
    {
        isLocallyFacingUp = b;
        if (isFaceingUp || isLocallyFacingUp)
            gameObject.GetComponent<SpriteRenderer>().sprite = sprite;
        else
            gameObject.GetComponent<SpriteRenderer>().sprite = backSprite;
    }


    public void OnSortingOrderChanged(int sortingOrder)
    {
        gameObject.GetComponent<SpriteRenderer>().sortingOrder = sortingOrder;
    }

    public override void DroppedOnZone(Zone zone)
    {
        lastParentZone = zone;
        ExSiblingIndex = transform.GetSiblingIndex();
        ExIsFacingUp = isFaceingUp;
    }

    [ClientRpc]
    public void RpcRestoreState(GameObject mParent, Vector3 mLocalPosition, int mSiblingIndex, int mSortingOrder, bool mIsFaceingUp)
    {
        transform.parent = mParent.transform;
        transform.SetSiblingIndex(mSiblingIndex);
        siblingIndex = mSiblingIndex;
        sortingOrder = mSortingOrder;
        isFaceingUp = mIsFaceingUp;
    }

    
}
