﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandCreateVote : Command  {
    private Player caller;
    private string text;

    public CommandCreateVote(string text)
    {
        this.text = text;
        this.caller = MyNetwork.Instance.Player;
    }

    public CommandCreateVote(Player caller, string text)
    {
        this.text = text;
        this.caller = caller;
    }

    public override void ExecuteOnAllClients()
    {
        CommandProcessor.Instance.CmdCreateVote(caller.gameObject, text);
    }

    public override void ExecuteOnClient(bool executeOnAllClients)
    {
    }

    public override void ExecuteServerCommand(bool executeOnAllClients)
    {
        base.ExecuteServerCommand(executeOnAllClients);
        VoteManager.Instance.CreateVoteOnServer(caller, text);
    }

    public override LogEntry GetLogEntry()
    {
        return new LogEntry(new LogEntryPart[] {
            new LogEntryPlayerPart(caller),
            new LogEntryPart(" created a vote : " + text)
        });
    }

    public override void UnExecuteServerCommand(bool executeOnAllClients)
    {
        base.UnExecuteServerCommand(executeOnAllClients);
    }
}

