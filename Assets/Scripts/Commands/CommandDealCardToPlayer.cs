﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CommandDealCardToPlayer : Command {

    private Player caller;
    private Player player;
    private CardView card;

    private Zone exHolder;
    private int exSiblingIndex;
    private bool exIsFacingUp;
    private bool isFacingUp;


    public CommandDealCardToPlayer(Player caller, Player player, CardView card)
    {
        this.caller = caller;
        this.card = card;
        this.player = player;
        exHolder = card.lastParentZone;
        exSiblingIndex = card.ExSiblingIndex;
        exIsFacingUp = card.ExIsFacingUp;
        isFacingUp = card.isFaceingUp;
    }

    public CommandDealCardToPlayer(CommandDealCardToPlayerMessage message)
    {
        this.caller = ClientScene.FindLocalObject(message.callerNetId).GetComponent<Player>();
        this.player = ClientScene.FindLocalObject(message.playerNetId).GetComponent<Player>();
        this.card = ClientScene.FindLocalObject(message.cardNetId).GetComponent<CardView>();
        this.Multi = message.multi;
        exHolder = card.lastParentZone;
        exSiblingIndex = card.ExSiblingIndex;
        exIsFacingUp = message.exIsFacingUp;
        isFacingUp = message.isFaceingUp;
    }


    public override void ExecuteOnAllClients()
    {
        CommandProcessor.Instance.CmdDealCardToPlayer(getCommandMessage());
    }


    public override void ExecuteServerCommand(bool executeOnAllClients)
    {
        base.ExecuteServerCommand(executeOnAllClients);
        CommandProcessor.Instance.RpcDealCardToPlayer(getCommandMessage(), executeOnAllClients);
    }

    public CommandDealCardToPlayerMessage getCommandMessage()
    {
        return new CommandDealCardToPlayerMessage(caller.netId, player.netId, card.netId, Multi, isFacingUp, exIsFacingUp);
    }

    public override void UnExecuteServerCommand(bool executeOnAllClients)
    {
        base.UnExecuteServerCommand(executeOnAllClients);
        CommandProcessor.Instance.RpcDropCardTo(new CommandDropCardToMessage(caller.netId, exHolder.netId, card.netId, exSiblingIndex, Multi, exIsFacingUp, isFacingUp), executeOnAllClients);
    }

    public override void ExecuteOnClient(bool executeOnAllClients)
    {
        if (!executeOnAllClients)
        {
            if (caller.isLocalPlayer) return;
        }

        player.DealCardToPlayer(card);
    }

    public override LogEntry GetLogEntry()
    {
        return new LogEntry(new LogEntryPart[] {
            new LogEntryPlayerPart(caller),
            new LogEntryPart(" dealt a card to everybody from "),
            new LogEntryZonePart(exHolder)
        });
    }
}


public class CommandDealCardToPlayerMessage : MessageBase, ICommandMessage
{
    public NetworkInstanceId callerNetId;
    public NetworkInstanceId playerNetId;
    public NetworkInstanceId cardNetId;
    public bool multi;
    internal bool exIsFacingUp;
    internal bool isFaceingUp;

    public CommandDealCardToPlayerMessage() { }

    public CommandDealCardToPlayerMessage(NetworkInstanceId callerNetId, NetworkInstanceId playerNetId, NetworkInstanceId cardNetId, bool multi, bool isFaceingUp, bool exIsFacingUp)
    {
        this.callerNetId = callerNetId;
        this.playerNetId = playerNetId;
        this.cardNetId = cardNetId;
        this.multi = multi;
        this.exIsFacingUp = exIsFacingUp;
        this.isFaceingUp = isFaceingUp;
    }

    public Command GetCommand()
    {
        return new CommandDealCardToPlayer(this);
    }
}
