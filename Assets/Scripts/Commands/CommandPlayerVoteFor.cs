﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandPlayerVoteFor : Command {
    private Player caller;
    private Vote.Options option;

    public CommandPlayerVoteFor(Vote.Options option)
    {
        this.caller = MyNetwork.Instance.Player;
        this.option = option;
    }

    public CommandPlayerVoteFor(Player caller, Vote.Options option)
    {
        this.caller = caller;
        this.option = option;
    }

    public override void ExecuteOnAllClients()
    {
        CommandProcessor.Instance.CmdPlayerVoteFor(caller.gameObject, option);
    }

    public override void ExecuteOnClient(bool executeOnAllClients)
    {
    }

    public override void ExecuteServerCommand(bool executeOnAllClients)
    {
        base.ExecuteServerCommand(executeOnAllClients);
        VoteManager.Instance.PlayerVoteFor(caller, option);
    }

    public override LogEntry GetLogEntry()
    {
        return new LogEntry(new LogEntryPart[] {
            new LogEntryPlayerPart(caller),
            new LogEntryPart(" voted for \'" + option.ToString()+"\'")
        });
    }

    public override void UnExecuteServerCommand(bool executeOnAllClients)
    {
        base.UnExecuteServerCommand(executeOnAllClients);
    }
}
