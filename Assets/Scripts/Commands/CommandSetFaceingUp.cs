﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CommandSetFacingUp : Command
{
    private Player caller;
    private CardView card;
    private bool isFacingUp;

    private bool exIsFacingUp;




    public CommandSetFacingUp(Player caller, CardView card, bool isFacingUp)
    {
        exIsFacingUp = card.ExIsFacingUp;
        this.caller = caller;
        this.card = card;
        this.isFacingUp = isFacingUp;
    }

    public CommandSetFacingUp(CommandSetFacingUpMessage message)
    {
        this.caller = ClientScene.FindLocalObject(message.callerNetId).GetComponent<Player>();
        this.card = ClientScene.FindLocalObject(message.cardNetId).GetComponent<CardView>();
        this.isFacingUp = message.isFacingUp;
        this.Multi = message.multi;
        exIsFacingUp = card.ExIsFacingUp;
    }


    public override void ExecuteOnAllClients()
    {
        CommandProcessor.Instance.CmdSetFacingUpdOnClients(getCommandMessage());
    }


    public override void ExecuteServerCommand(bool executeOnAllClients)
    {
        base.ExecuteServerCommand(executeOnAllClients);
        CommandProcessor.Instance.RpcSetFacingUpdOnClients(getCommandMessage(), executeOnAllClients);
    }

    public CommandSetFacingUpMessage getCommandMessage()
    {
        return new CommandSetFacingUpMessage(caller.netId, card.netId, isFacingUp, Multi);
    }

    public override void UnExecuteServerCommand(bool executeOnAllClients)
    {
        base.UnExecuteServerCommand(executeOnAllClients);
        CommandProcessor.Instance.RpcSetFacingUpdOnClients(new CommandSetFacingUpMessage(caller.netId, card.netId, exIsFacingUp, Multi), executeOnAllClients);
    }

    public override void ExecuteOnClient(bool executeOnAllClients)
    {
        if (!executeOnAllClients)
        {
            if (caller.isLocalPlayer) return;
        }

        card.SetFacingUp(isFacingUp);
    }

    public override LogEntry GetLogEntry()
    {
        return  new LogEntry(new LogEntryPart[] {
            new LogEntryPlayerPart(caller),
            new LogEntryPart(" flipped a card ")
        });
    }
}


public class CommandSetFacingUpMessage : MessageBase, ICommandMessage
{
    public NetworkInstanceId callerNetId;
    public NetworkInstanceId cardNetId;
    public bool isFacingUp;
    public bool multi;


    public CommandSetFacingUpMessage() { }

    public CommandSetFacingUpMessage(NetworkInstanceId callerNetId, NetworkInstanceId cardNetId, bool isFacingUp, bool multi)
    {
        this.callerNetId = callerNetId;
        this.cardNetId = cardNetId;
        this.isFacingUp = isFacingUp;
        this.multi = multi;
    }

    public Command GetCommand()
    {
        return new CommandSetFacingUp(this);
    }
}
