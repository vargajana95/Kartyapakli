﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandSetZoneOwner : Command
{
    private Player caller;
    private Zone zone;
    private Player owner;

    public CommandSetZoneOwner(Zone zone, Player owner)
    {
        this.caller = MyNetwork.Instance.Player;
        this.zone = zone;
        this.owner = owner;
    }

    public CommandSetZoneOwner(Player caller, Zone zone, Player owner)
    {
        this.caller = caller;
        this.zone = zone;
        this.owner = owner;
    }

    public override void ExecuteOnAllClients()
    {
        CommandProcessor.Instance.CmdSetZoneOwner(caller.gameObject, zone.gameObject, owner.gameObject);
    }

    public override void ExecuteOnClient(bool executeOnAllClients)
    {
    }

    public override void ExecuteServerCommand(bool executeOnAllClients)
    {
        base.ExecuteServerCommand(executeOnAllClients);
        zone.ownerNetId = owner.netId;
    }

    public override LogEntry GetLogEntry()
    {
        return new LogEntry(new LogEntryPart[] {
            new LogEntryPlayerPart(caller),
            new LogEntryPart(" took ownership of "),
            new LogEntryZonePart(zone)

        });
    }

    public override void UnExecuteServerCommand(bool executeOnAllClients)
    {
        base.UnExecuteServerCommand(executeOnAllClients);
    }
}
