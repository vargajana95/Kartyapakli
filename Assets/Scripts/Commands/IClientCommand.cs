﻿using UnityEngine;

public interface IClientCommand {
    void ExecuteOnAllClients();

    void ExecuteOnClient(bool executeOnAllClients);
	
}
