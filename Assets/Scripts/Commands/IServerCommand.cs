﻿public interface IServerCommand {
    bool Multi { get; set; }


    void ExecuteServerCommand(bool executeOnAllClients);
    void UnExecuteServerCommand(bool executeOnAllClients);
}
