﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ContextMenu {

    public class ContextMenuItem
    {

        public UnityAction action;
        public string text;

        public ContextMenuItem(string text, UnityAction action)
        {
            this.text = text;
            this.action = action;
        }
    }

    private List<ContextMenuItem> menuItems = new List<ContextMenuItem>();

    public void AddMenuItem(ContextMenuItem menuItem)
    {
        menuItems.Add(menuItem);
    }

    public List<ContextMenuItem> GetMenuItems()
    {
        return menuItems;
    }
}
