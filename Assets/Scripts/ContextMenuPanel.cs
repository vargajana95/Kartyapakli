﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ContextMenuPanel : MonoBehaviour {

    public GameObject contextMenuObject;

    private static ContextMenuPanel contextMenuPanel;

    public Button contextMenuItemToSpawn;

    public static ContextMenuPanel Instance()
    {
        if (!contextMenuPanel)
        {
            contextMenuPanel = FindObjectOfType(typeof(ContextMenuPanel)) as ContextMenuPanel;
            if (!contextMenuPanel)
                Debug.LogError("There needs to be one active ContextMenuPanel script on a GameObject in your scene.");
        }

        return contextMenuPanel;
    }

    public void Show(Vector3 positon, ContextMenu contextMenu)
    {
        contextMenuObject.transform.parent.gameObject.SetActive(true);
        foreach (Transform child in contextMenuObject.transform)
        {
            Destroy(child.gameObject);
        }
        contextMenuObject.transform.position = positon;

        var menuItems = contextMenu.GetMenuItems();
        foreach (var menuItem in menuItems)
        {
            Button contextMenuItem = Instantiate(contextMenuItemToSpawn, contextMenuObject.transform);
            contextMenuItem.GetComponentInChildren<Text>().text = menuItem.text;
            contextMenuItem.onClick.AddListener(menuItem.action);
            contextMenuItem.onClick.AddListener(Close);
        }

    }

    public void Close()
    {
        contextMenuObject.transform.parent.gameObject.SetActive(false);
    }
}
