﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(IHasContextMenu))]
public class ContextMenuShow : MonoBehaviour {

    private IHasContextMenu hasContextMenu;


    // Use this for initialization
    void Start () {
        hasContextMenu = GetComponent<IHasContextMenu>();

        hasContextMenu.InitContextMenu();
    }
	
    public void ShowContextMenu(Vector3 position)
    {
        ContextMenuPanel.Instance().Show(position, hasContextMenu.GetContextMenu());
    }
}
