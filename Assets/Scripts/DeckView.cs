﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
using DG.Tweening;
using System;

public class DeckView : Zone
{
    private Vector3 pointerOffset;
    public CardView card;
    //public List<Sprite> cardList;
    //public List<Sprite> cardList;
    private List<GameObject> cards;
    private List<int> cardNums = new List<int>();

    public DeckInfo DeckInfo { get; set; }
    public DeckData DeckData { get; set; }

    public List<int> CardNums
    {
        get
        {
            return cardNums;
        }

        set
        {
            cardNums = value;
        }
    }


    protected override void Start()
    {
        base.Start();
        /*if (isServer)
        {
            Shuffle(cardList);
            for (int i = 0; i < cardList.Count; i++)
            {
                CardView cardToSpawn = Instantiate(card, transform.position, transform.rotation);
                cardToSpawn.cardSpriteName = Int32.Parse( cardList[i].name.Split('_')[1]);
                //o.transform.SetParent(this.transform);
                cardToSpawn.parent = this.gameObject;
                cardToSpawn.sortingOrder = i;
                //cardView.colliderEnabled = false;
                NetworkServer.Spawn(cardToSpawn.gameObject);
                cardNums.Add(i);
            }
        }*/

        if (isServer)
        {
            InitDeck();
        }
            
    }

    

    [Server]
    public void InitDeck()
    {
        int sortingOrder = 0;
        List<int> cardList = DeckInfo.cards;
        //Shuffle(cardList);
        for (int i = 0; i < cardList.Count; i++)
        {
            for (int j = 0; j < DeckData.cardNums[i]; j++)
            {
                CardView cardToSpawn = Instantiate(card, transform.position, transform.rotation);
                //cardToSpawn.cardSpriteName = Int32.Parse(cardList[i].name.Split('_')[1]);
                cardToSpawn.cardSpriteIndex = cardList[i];
                cardToSpawn.cardSpritePath = DeckInfo.deckImagePath;
                cardToSpawn.backSpritePath = DeckInfo.deckBackPath;
                //o.transform.SetParent(this.transform);
                cardToSpawn.parent = this.gameObject;
                cardToSpawn.sortingOrder = sortingOrder;
                //cardView.colliderEnabled = false;
                NetworkServer.Spawn(cardToSpawn.gameObject);
                cardNums.Add(sortingOrder);
                sortingOrder++;
            }
            
        }
    }

    

    [Server]
    public void ShuffleCards()
    {
        cardNums.Clear();
        int num = 0;
        foreach (Transform child in transform)
        {
            cardNums.Add(num++);
        }

        Shuffle(cardNums);

        SetCardOrders(cardNums);
    }

    [Server]
    public void SetCardOrders(List<int> cardNumsParam)
    {
        this.cardNums = cardNumsParam;
        List<Transform> childs = new List<Transform>();
        foreach (Transform child in transform)
        {
            childs.Add(child);
        }
        transform.DetachChildren();

        for (int i = 0; i < childs.Count; i++)
        {
            childs[cardNums[i]].SetParent(this.transform);
            //Debug.Log(childs[cardNums[i]].GetComponent<CardView>().cardSpriteIndex);
            childs[cardNums[i]].GetComponent<CardView>().sortingOrder = i;
            childs[cardNums[i]].GetComponent<CardView>().siblingIndex = i;
        }
    }

    //https://stackoverflow.com/questions/273313/randomize-a-listt
    private static System.Random rng = new System.Random();

    private void Shuffle<T>(IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    /*public override void PlaceDraggableEntity(DraggableEntity entity)
    {
        entity.lastParentZone = this;
        entity.transform.SetParent(this.transform);
        entity.transform.DOLocalMove(Vector3.zero, 0.2f);
        entity.GetComponent<SpriteRenderer>().sortingOrder = this.transform.childCount;
    }*/

    public override void PlaceDraggableEntity(DraggableEntity entity)
    {
        entity.transform.SetParent(this.transform);
        entity.transform.DOLocalMove(Vector3.zero, 0.2f);
        UpdateCardsSortingOrder();
    }

    public override void InitContextMenu()
    {
        base.InitContextMenu();
        GetContextMenu().AddMenuItem(new ContextMenu.ContextMenuItem("Shuffle", () => { CommandProcessor.Instance.ExecuteClientCommand(new CommandShuffleDeck(MyNetwork.Instance.Player, this)); }));
        GetContextMenu().AddMenuItem(new ContextMenu.ContextMenuItem("Deal", () => { DealCardsToPlayers(); }));
    }


    private void DealCardsToPlayers()
    {
        if (this.transform.childCount <= 0)
            return;

        var players = GameObject.FindGameObjectsWithTag("Player");

        int i = 1;
        foreach (var player in players)
        {
            //MyNetwork.Instance.DealToPlayer(player.GetComponent<Player>(), this.transform.GetChild(this.transform.childCount - i++).GetComponent<DraggableEntity>());
            CommandProcessor.Instance.ExecuteClientCommand(new CommandDealCardToPlayer(MyNetwork.Instance.Player, player.GetComponent<Player>(), this.transform.GetChild(this.transform.childCount - i++).GetComponent<CardView>()));
        }
    }




}

