﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
using DG.Tweening;

[RequireComponent(typeof(BoxCollider2D))]
public abstract class DraggableEntity : Entity, IBeginDragHandler, IDragHandler, IEndDragHandler, IDroppable
{
    private BoxCollider2D boxCollider;

    public Vector3 lastPosition;
    public Zone lastParentZone;

    public Transform FutureParent { get; set; }

    public bool IsDraggable { get; set; }

    public int ExSiblingIndex { get; set; }


    protected override void Start()
    {
        base.Start();
        boxCollider = GetComponent<BoxCollider2D>();

        lastPosition = this.transform.position;
        if (this.transform.parent != null)
            lastParentZone = this.transform.parent.GetComponent<Zone>();

    }

    public virtual void OnBeginDrag(PointerEventData eventData)
    {
        boxCollider.enabled = false;


        FutureParent = transform.parent;
        if (FutureParent != null)
        {
            IDropZone dropZone = FutureParent.GetComponent<IDropZone>();
            if (dropZone != null)
                dropZone.OnItemBeginDrag(this);
        }

        if (!IsDraggable)
        {
            eventData.pointerDrag = null;
            return;

        }

    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        if (FutureParent != null)
        {
            IDropZone dropZone = FutureParent.GetComponent<IDropZone>();
            if (dropZone != null)
                dropZone.OnItemDrag(this);

           
        }
    }

    public abstract void DroppedOnZone(Zone zone);

    public virtual void OnEndDrag(PointerEventData eventData)
    {
        boxCollider.enabled = true;

        if (FutureParent != null)
        {
            IDropZone dropZone = FutureParent.GetComponent<IDropZone>();
            if (dropZone != null)
                dropZone.OnItemEndDrag(this);
        }

        IsDraggable = false;
    }

    public virtual void OnDrop()
    {
    }

    public void GoBackToLastPosition()
    {
        this.transform.SetParent(lastParentZone.transform);
        this.transform.DOLocalMove(lastPosition, 0.2f);
    }

    public void ReturnToLastZone()
    {
        lastParentZone.DropCardOnClients(this, ExSiblingIndex);
    }
}
