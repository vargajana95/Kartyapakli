﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

public class Entity : NetworkBehaviour, IPointerUpHandler, IPointerDownHandler
{
    private static float longTouchTime = 0.7f;
    private bool isPointerDown = false;
    private bool longPressTriggered = false;
    private float timePressStarted;

    [SyncVar]
    public GameObject parent;

    //TODO use ethis everywhere!!!
    [SyncVar]
    public Vector3 localPosition;



    protected virtual void Start()
    {

        /*if (parent == null)
            this.transform.SetParent(null);
        else*/
        if (parent != null)
            this.transform.SetParent(parent.transform);

        //TODO delete if
        if (!localPosition.Equals(Vector3.zero))
            this.transform.localPosition = localPosition;
    }

    private void Update()
    {
        if (isPointerDown && !longPressTriggered)
        {
            if (Time.time - timePressStarted > longTouchTime)
            {
                longPressTriggered = true;
                OnLongTouch();
            }
        }
    }


    public void OnPointerDown(PointerEventData eventData)
    {
        timePressStarted = Time.time;
        isPointerDown = true;
        longPressTriggered = false;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (isPointerDown && !longPressTriggered)
        {
            if (Time.time - timePressStarted <= longTouchTime)
            {
                OnTouch();
            }
        }
        isPointerDown = false;
    }


    public void OnPointerExit(PointerEventData eventData)
    {
        isPointerDown = false;
    }



    public virtual void OnTouch()
    {
    }

    public virtual void OnLongTouch()
    {
    }
}
