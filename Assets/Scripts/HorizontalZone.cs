﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

public class HorizontalZone : Zone {

    GameObject placeholder;

    [SyncVar]
    public Permission.AllowType viewPermissionType;

    public Permission ViewPermission { get; set; }


    protected override void Start()
    {
        base.Start();
        //this.transform.localPosition = Vector3.zero;
    }



    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);

        if (eventData.pointerDrag != null)
        {
            CreatePlaceHolder(eventData.pointerDrag.transform);
        }
    }

    private void CreatePlaceHolder(Transform objectToClone)
    {
        if (placeholder == null)
        {
            placeholder = new GameObject("Placeholder");
            placeholder.AddComponent<SpriteRenderer>();
            placeholder.transform.SetParent(this.transform);
            placeholder.transform.position = objectToClone.position;
            placeholder.transform.SetSiblingIndex(objectToClone.GetSiblingIndex());
        }
    }

    private void DestroyPlaceholder()
    {
        if (placeholder != null)
        {
            placeholder.transform.parent = null;
            Destroy(placeholder);
            placeholder = null;
        }
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);

        if (eventData.pointerDrag != null)
        {
            DestroyPlaceholder();

            UpdateCardPositions();
        }
    }

    public override void OnItemBeginDrag(DraggableEntity draggedItem)
    {
        base.OnItemBeginDrag(draggedItem);

        CreatePlaceHolder(draggedItem.transform);
    }

    public override void OnItemDrag(DraggableEntity draggedItem)
    {
        base.OnItemDrag(draggedItem);

        int newIndex = transform.childCount;
        for (int i = 1; i < transform.childCount; i++)
        {
            if (draggedItem.transform.position.x < transform.GetChild(i).position.x)
            {
                newIndex = i;

                if (placeholder.transform.GetSiblingIndex() < newIndex)
                    newIndex--;

                break;
            }
        }

        placeholder.transform.SetSiblingIndex(newIndex);

        UpdateCardPositions();

        draggedItem.GetComponent<SpriteRenderer>().sortingOrder = placeholder.GetComponent<SpriteRenderer>().sortingOrder;
    }

    public override void OnItemEndDrag(DraggableEntity draggedItem)
    {
        base.OnItemEndDrag(draggedItem);

        /*if (placeholder != null)
        {
            //draggedItem.GetComponent<CardView>().siblingIndex = placeholder.transform.GetSiblingIndex();
            draggedItem.transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());
        }


        DestroyPlaceholder();

        if (ownerNetId != MyNetwork.Instance.Player.netId)
        {
            return;
        }

        //draggedItem.transform.SetParent(transform);
        MyNetwork.Instance.DropCardTo(this.gameObject, draggedItem.gameObject, draggedItem.transform.GetSiblingIndex());

        draggedItem.GetComponent<CardView>().SetLocallyFacingUp(true);

        UpdateCardPositions();*/
    }

    public override void OnItemDropped(DraggableEntity entity)
    {
        base.OnItemDropped(entity);

        if (placeholder != null)
        {
            //draggedItem.GetComponent<CardView>().siblingIndex = placeholder.transform.GetSiblingIndex();
            entity.transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());
        }


        DestroyPlaceholder();



        //draggedItem.transform.SetParent(transform);
        //MyNetwork.Instance.DropCardTo(this.gameObject, entity.gameObject, entity.transform.GetSiblingIndex());
        CommandProcessor.Instance.ExecuteClientCommand(new CommandDropCardTo(MyNetwork.Instance.Player, this, entity.GetComponent<CardView>(), entity.transform.GetSiblingIndex()));


        if (ownerNetId == MyNetwork.Instance.Player.netId)
            entity.GetComponent<CardView>().SetLocallyFacingUp(true);

        UpdateCardPositions();

        MoveCardsInside();
        entity.DroppedOnZone(this);
    }

    private void UpdateCardPositions()
    {
        Vector3 startingPosition = new Vector3(GetComponent<SpriteRenderer>().sprite.bounds.size.x / 2, 0, 0);

        //First child is the glow
        for (int i = 1; i < transform.childCount; i++)
        {
            //transform.GetChild(i).transform.localPosition = -startingPosition + new Vector3(/*transform.GetChild(i).GetComponent<SpriteRenderer>().sprite.bounds.size.x / 2*/ 1.5f + i * 1f, 0, 0);
            var newPos = -startingPosition + new Vector3(1f + (i - 1) * 0.8f, 0, 0);
            transform.GetChild(i).DOLocalMove(newPos, 0.1f);
            transform.GetChild(i).GetComponent<SpriteRenderer>().sortingOrder = i - 1;
        }
    }

    


    public override void DropCardOnClients(DraggableEntity card, int siblingIndex = -1)
    {
        base.DropCardOnClients(card, siblingIndex);

        if (ownerNetId == MyNetwork.Instance.Player.netId)
            card.GetComponent<CardView>().SetLocallyFacingUp(true);

        UpdateCardPositions();

    }

    public override void OnCardRemoved()
    {
        base.OnCardRemoved();
        UpdateCardPositions();
    }

    public override void PlaceDraggableEntity(DraggableEntity entity)
    {
        entity.transform.SetParent(this.transform);
        /*UpdateCardPositions();
        MoveCardsInside();*/

        //MyNetwork.Instance.DropCardTo(this.gameObject, entity.gameObject, entity.transform.GetSiblingIndex());
        //CommandProcessor.Instance.ExecuteClientCommand(new CommandDropCardTo(MyNetwork.Instance.Player, this, entity.GetComponent<CardView>(), entity.transform.GetSiblingIndex()));

    }
}
