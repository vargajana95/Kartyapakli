﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public interface IDropZone : IDropHandler, IPointerEnterHandler, IPointerExitHandler {

    //Called when something is being dragged over
    void OnItemBeginDrag(DraggableEntity draggedItem);

    void OnItemDrag(DraggableEntity draggedItem);

    void OnItemEndDrag(DraggableEntity draggedItem);

    void OnCardRemoved();

}
