﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHasContextMenu  {

    void InitContextMenu();

    ContextMenu GetContextMenu();
}
