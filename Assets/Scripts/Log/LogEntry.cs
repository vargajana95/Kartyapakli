﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogEntry
{
    public List<LogEntryPart> Parts { get; private set; }

    private LogEntryData data;

    public LogEntryData Data
    {
        get
        {
            return data;
        }
    }

    public string TypeText
    {
        get
        {
            return data.typeText;
        }
        set
        {
            data.typeText = value;
        }
    }
    public bool Active
    {
        get
        {
            return data.active;
        }
        set
        {
            data.active = value;
        }
    }
    public string Text
    {
        get
        {
            string t = string.IsNullOrEmpty(TypeText) ? "" : TypeText + ": ";
            if (Active)
                return t + data.text;
            else
                return t + "<i>" +data.text+"</i>";
        }
        set
        {
            data.text = value;
        }
    }

    public LogEntry(LogEntryPart part)
    {
        Parts = new List<LogEntryPart>
        {
            part
        };

        Text = PartsToText();
        Active = true;
    }


    public LogEntry(LogEntryPart[] parts)
    {
        this.Parts = new List<LogEntryPart>(parts);
        Text = PartsToText();
        Active = false;
    }

    public LogEntry(string text)
    {
        Text = text;
    }

    public LogEntry(LogEntryData data)
    {
        this.data = data;
    }

    private string PartsToText()
    {
        string text =  "";
        foreach (var part in Parts)
        {
            text += part.Text;
        }

        return text;
    }
}

public struct LogEntryData
{
    public bool active;
    public string text;
    public string typeText;

    public LogEntryData(string text, string typeText, bool active = false)
    {
        this.active = active;
        this.text = text;
        this.typeText = typeText;
    }
}
