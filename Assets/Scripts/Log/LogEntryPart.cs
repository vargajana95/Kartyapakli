﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogEntryPart {
    public Color TextColor { get; set; }
    public string Text { get; protected set; }

    public LogEntryPart(string text)
    {
        TextColor = Color.white;
        Text = text;
    }


}

public class LogEntryZonePart : LogEntryPart
{
    public LogEntryZonePart(Zone zone) : base(zone.zoneName)
    {
        TextColor = zone.zoneColor;

        string text = "";
        text += "<b><color=#" + ColorUtility.ToHtmlStringRGBA(TextColor) + ">";
        text += Text;
        text += "</color></b>";
        Text = text;
    }

}

public class LogEntryPlayerPart : LogEntryPart
{
    public LogEntryPlayerPart(Player player) : base(player.playerName)
    {
        string text = "";
        text += "<b>";
        text += Text;
        text += "</b>";
        Text = text;
    }

}
