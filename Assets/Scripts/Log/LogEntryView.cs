﻿using UnityEngine;
using UnityEngine.UI;

public class LogEntryView : MonoBehaviour { 

    [SerializeField]
    private Text textView;

    public void InitView(LogEntry entry)
    {
        textView.text = entry.Text;
    }

}


