﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogPanel : Menu<LogPanel> {

    [SerializeField]
    private GameObject entryList;

    [SerializeField]
    private Scrollbar scrollbar;

    [SerializeField]
    private LogEntryView entryPrefab;

    public static void Show()
    {
        Open(new LogPanelCreator());

        Instance.InitEntries();
    }

    private void InitEntries()
    {

        foreach (var entry in LogManager.Instance.Entries)
        {
            LogEntryView newEntry = Instantiate(entryPrefab, entryList.transform);
            newEntry.InitView(entry);
        }

    }
    public static void Hide()
    {
        Close();
    }

    public void Start()
    {
        scrollbar.value = 0;
    }

    public void OnCancelPressed()
    {
        Hide();
    }

    public class LogPanelCreator : MenuManager.IMenuCreator
    {
        public void InstanciateMenu(Transform parent)
        {
            Instantiate(MenuManager.Instance.logPanelPrefab, parent);
        }
    }

}
