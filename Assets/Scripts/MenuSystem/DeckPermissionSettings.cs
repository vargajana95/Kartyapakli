﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeckPermissionSettings : Menu<DeckPermissionSettings> {
    public RadioGroup takeAwayPermissionGroup;
    public InputField nameInputField;
    [SerializeField]
    private Button colorButton;

    private DeckEditor deck;

    private static Color[] Colors = new Color[] { Color.magenta, Color.red, Color.cyan, Color.blue, Color.green, Color.yellow };
    private int colorIdx;



    public static void Show(DeckEditor deck)
    {
        Open(new DeckPermissionSettingsCreator());
        Instance.deck = deck;
        Instance.nameInputField.text = deck.Name;
        Instance.colorButton.GetComponent<Image>().color = deck.Color;
        Instance.colorIdx = Array.FindIndex(Colors, (color) => color.Equals(deck.Color));

        Instance.takeAwayPermissionGroup.SetSelectedRadioButton((byte)deck.TakeAwayPermissionType);

    }
    public static void Hide()
    {
        Close();
    }

    public void OnCancelPressed()
    {
        Hide();
    }

    public void OnOkButtonPressed()
    {
        deck.TakeAwayPermissionType = (Permission.AllowType) takeAwayPermissionGroup.GetSelectedRadioButtonIndex();
        deck.Name = nameInputField.text;
        deck.Color = Colors[colorIdx];

        Hide();
    }

    public void OnColorButtonClicked()
    {
        colorIdx = (colorIdx + 1) % Colors.Length;

        colorButton.GetComponent<Image>().color = Colors[colorIdx];
    }

    public class DeckPermissionSettingsCreator : MenuManager.IMenuCreator
    {
        public void InstanciateMenu(Transform parent)
        {
            Instantiate(MenuManager.Instance.deckPermissionSettingsPrefab, parent);
        }
    }
}
