﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsMenu : Menu<SettingsMenu> {

    public static void Show() {
        MenuManager.Instance.CreateInstance(new SettingsMenuCreator());
    }



    public class SettingsMenuCreator : MenuManager.IMenuCreator
    {
        public void InstanciateMenu(Transform parent)
        {
            Instantiate(MenuManager.Instance.deckSettingsPrefab, parent);
        }
    }

}
