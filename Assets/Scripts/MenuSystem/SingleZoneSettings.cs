﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SingleZoneSettings : Menu<SingleZoneSettings> {
    public RadioGroup takeAwayPermissionGroup;
    public RadioGroup dropOntoPermissionGroup;
    public RadioGroup viewPermissionGroup;
    public InputField nameInputField;

    private SingleZoneEditor zone;

    [SerializeField]
    private Button colorButton;
    private static Color[] Colors = new Color[] { Color.magenta, Color.red, Color.cyan, Color.blue, Color.green, Color.yellow };
    private int colorIdx;


    public static void Show(SingleZoneEditor zone)
    {
        Open(new ZoneSettingsCreator());
        Instance.zone = zone;
        Instance.nameInputField.text = zone.Name;
        Instance.colorButton.GetComponent<Image>().color = zone.Color;
        Instance.colorIdx = Array.FindIndex(Colors, (color) => color.Equals(zone.Color));

        Instance.takeAwayPermissionGroup.SetSelectedRadioButton((byte)zone.TakeAwayPermissionType);
        Instance.dropOntoPermissionGroup.SetSelectedRadioButton((byte)zone.DropOntoPermissionType);
        Instance.viewPermissionGroup.SetSelectedRadioButton((byte)zone.ViewPermissionType);

    }
    public static void Hide()
    {
        Close();
    }

    public void OnCancelPressed()
    {
        Hide();
    }

    public void OnOkButtonPressed()
    {
        zone.TakeAwayPermissionType = (Permission.AllowType) takeAwayPermissionGroup.GetSelectedRadioButtonIndex();
        zone.DropOntoPermissionType = (Permission.AllowType) dropOntoPermissionGroup.GetSelectedRadioButtonIndex();
        zone.ViewPermissionType = (Permission.AllowType) viewPermissionGroup.GetSelectedRadioButtonIndex();
        zone.Name = nameInputField.text;
        zone.Color = Colors[colorIdx];

        Hide();
    }

    public void OnColorButtonClicked()
    {
        colorIdx = (colorIdx + 1) % Colors.Length;

        colorButton.GetComponent<Image>().color = Colors[colorIdx];
    }

    public class ZoneSettingsCreator : MenuManager.IMenuCreator
    {
        public void InstanciateMenu(Transform parent)
        {
            Instantiate(MenuManager.Instance.zoneSettingsPrefab, parent);
        }
    }
}
