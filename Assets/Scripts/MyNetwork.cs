﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class MyNetwork {

    private static MyNetwork instance;

    public Player Player { get; set; }

    private MyNetwork() { }

    public static MyNetwork Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new MyNetwork();
            }
            return instance;
        }
    }


    
}
