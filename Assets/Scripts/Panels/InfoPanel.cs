﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoPanel : MonoBehaviour {
    public Text textField;


    public void SetInfo(string info, float hideAfterSeconds = 0)
    {
        this.gameObject.SetActive(true);

        this.textField.text = info;

        if (hideAfterSeconds > 0)
        {
            StartCoroutine(ClosePanelAfter(hideAfterSeconds));
        }
    }

    private IEnumerator ClosePanelAfter(float seconds)
    {
        float remainingTime = seconds;

        while (remainingTime > 0)
        {
            yield return null;

            remainingTime -= Time.deltaTime;
        }

        ClosePanel();
    }

    public void ClosePanel()
    {
        this.gameObject.SetActive(false);
    }
}
