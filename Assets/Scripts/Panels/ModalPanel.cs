﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

//  This script will be updated in Part 2 of this 2 part series.
public class ModalPanel : MonoBehaviour
{

    public Text textField;
    public Button yesButton;
    public Button noButton;
    public QuestionPanel questionPanel;
    public InfoPanel infoPanel;

    private static ModalPanel modalPanel;

    public static ModalPanel Instance()
    {
        if (!modalPanel)
        {
            modalPanel = FindObjectOfType(typeof(ModalPanel)) as ModalPanel;
            if (!modalPanel)
                Debug.LogError("There needs to be one active ModalPanel script on a GameObject in your scene.");
        }

        return modalPanel;
    }

    // Yes/No/Cancel: A string, a Yes event, a No event and Cancel event
    public void Info(string info, float hideAfterSeconds = 0)
    {
        infoPanel.SetInfo(info, hideAfterSeconds);

    }
    public void Choice(string question, UnityAction yesEvent, UnityAction noEvent)
    {
        questionPanel.SetQuestion(question, yesEvent, noEvent);
    }

    public void CloseQuestionPanel()
    {
        questionPanel.ClosePanel();
    }

    public void CloseInfoPanel()
    {
        infoPanel.ClosePanel();
    }


}
