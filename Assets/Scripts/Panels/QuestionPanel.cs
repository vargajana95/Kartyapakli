﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class QuestionPanel : MonoBehaviour {
    public Text textField;
    public Button yesButton;
    public Button noButton;

    public void SetQuestion(string question, UnityAction yesAction, UnityAction noAction)
    {
        this.gameObject.SetActive(true);

        yesButton.onClick.RemoveAllListeners();
        yesButton.onClick.AddListener(yesAction);
        yesButton.onClick.AddListener(ClosePanel);

        noButton.onClick.RemoveAllListeners();
        noButton.onClick.AddListener(noAction);
        noButton.onClick.AddListener(ClosePanel);



        this.textField.text = question;

        yesButton.gameObject.SetActive(true);
        noButton.gameObject.SetActive(true);
    }

    public void ClosePanel()
    {
        this.gameObject.SetActive(false);
    }
}
