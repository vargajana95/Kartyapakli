﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

public class Permission {

    public enum AllowType
    {
        DENY = 0, ALLOW, WARNING, VOTE
    }

    public enum AccessType
    {
        EVERYBODY, OWNERONLY
    }

    //private AccessType accessType;
    public AllowType Type { get; set; }


    public Permission(AllowType allowType/*, AccessType accessType*/)
    {
        this.Type = allowType;
        //this.accessType = accessType;
    }

    public void Check(Player player, NetworkInstanceId ownerNetId, UnityAction yesEvent, UnityAction noEvent, string question = "")
    {
        /*if (accessType == Permission.AccessType.OWNERONLY)
        {
            if (allowType == Permission.AllowType.DENY)
            {
                if (ownerNetId == player.netId) yesEvent();
                else noEvent();
            }
            else if (allowType == Permission.AllowType.WARNING)
            {
                if (ownerNetId != player.netId)
                {
                    ModalPanel.Instance().Choice("Are you sure?",
                        yesEvent,
                        noEvent);
                }
                else yesEvent();
            }
        }
        else if (accessType == Permission.AccessType.EVERYBODY)
        {
            yesEvent();
        }*/
        if (ownerNetId == player.netId)
        {
            yesEvent();
        }
        else if (Type == AllowType.ALLOW)
        {
            yesEvent();

        }
        else if (Type == AllowType.DENY)
        {
            noEvent();
        }
        else if (Type == AllowType.WARNING)
        {
            ModalPanel.Instance().Choice(question,
                        yesEvent,
                        noEvent);
        }
        else if (Type == AllowType.VOTE)
        {
            VoteManager.Instance.CreateVote(question, yesEvent, noEvent);
        }
        else
        {
            noEvent();
        }

           
    }


}
