﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Player : NetworkBehaviour {

    public HorizontalZone handPrefab;
    public Button playerNameButtonPrefab;
    public Table tablePrefab;

    [SyncVar]
    private NetworkInstanceId handId;

    [SyncVar]
    private NetworkInstanceId tableId;

    private HorizontalZone hand;
    private Table table;

    [SyncVar]
    public string playerName;

    public Color Color { get; set; }


    void Start()
    {
        Debug.Log("Start");
        /*if (isServer)
        {
            //TODO put this to OnStartServer()?
            SpawnHandAndTable();
        }*/


        
        SetHandAndTable();

        

        if (!isLocalPlayer)
        {
            //this.transform.position = new Vector3(0, 12.5f, 0);

            var playerNamePanel = GameObject.FindGameObjectWithTag("PlayerNamePanel");
            if (playerNamePanel != null)
            {
                var playerNameButton = Instantiate(playerNameButtonPrefab, playerNamePanel.transform);
                playerNameButton.onClick.AddListener(() => { GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraMove>().MoveCameraToTable(this); });
                playerNameButton.GetComponentInChildren<Text>().text = playerName;
            }

            /*var tableObject = ClientScene.FindLocalObject(tableId);
            if (tableObject != null)
            {
                table = tableObject.GetComponent<Table>();
                table.transform.SetParent(this.transform);

                table.transform.position = new Vector3(0, 10, 0);
            }

            var handObject = ClientScene.FindLocalObject(handId);
            if (handObject != null) {
                //Set the hand on clients that join earlier

                hand = handObject.GetComponent<HorizontalZone>();
                hand.transform.localPosition = new Vector3(0, 12.5f, -0.5f);
                hand.transform.SetParent(table.transform);

            }*/
         

        }



    }

    public override void OnStartServer()
    {
        Debug.Log("OnStartServer");
        SpawnHandAndTable();
    }

    public override void OnStartClient()
    {
        /*Debug.Log("OnStartClient");
        Debug.Log(isLocalPlayer);
        SetHandAndTable();*/
    }



    public void MoveTableToBack()
    {
        /*Vector3 newPos = table.transform.localPosition;
        newPos.z = 1;
        table.transform.localPosition = newPos;*/
        table.gameObject.SetActive(false);
    }

    public void MoveTableToFront()
    {
        /*Vector3 newPos = table.transform.localPosition;
        newPos.z = -1;
        table.transform.localPosition = newPos;*/
        table.gameObject.SetActive(true);
    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();

        Debug.Log("OnStartLocalPlayer");
        MyNetwork.Instance.Player = this;

        //this.transform.position = new Vector3(0, -2.7f, 0);
    }


    void SpawnHandAndTable()
    {
        //hand.transform.localPosition.Set(0, -2.7f, 0);
        hand = Instantiate(handPrefab, this.transform);
        //hand.parent = this.gameObject;
        hand.ownerNetId = this.netId;
        hand.zoneName = playerName + "'s hand";
        hand.zoneColor = this.Color;


        NetworkServer.Spawn(hand.gameObject);
        handId = hand.netId;

        table = Instantiate(tablePrefab, this.transform);
        NetworkServer.Spawn(table.gameObject);
        tableId = table.netId;

        TableEditorDataHolder tableEditorManager = GameObject.FindGameObjectWithTag("TableEditorDataHolder").GetComponent<TableEditorDataHolder>();


        tableEditorManager.PlayerTable.SpawnEntities(table.gameObject);


        //TargetSpawnHand(connectionToClient);

        //RpcSpawnHand(hand.netId);
    }

    //[ClientRpc]
    private void SetHandAndTable()
    {
        //Set the hand and table on the client just connected

        var tableObject = ClientScene.FindLocalObject(tableId);
        if (tableObject != null)
        {
            table = tableObject.GetComponent<Table>();
            table.transform.SetParent(this.transform);
            if (isLocalPlayer)
            {
                table.transform.position = new Vector3(0, -10, 0);
            }
            else
            {
                table.transform.position = new Vector3(0, 10, 0);
            }
        }

        var handObject = ClientScene.FindLocalObject(handId);
        if (handObject != null)
        {
            hand = handObject.GetComponent<HorizontalZone>();
            if (isLocalPlayer)
            {
                hand.transform.SetParent(GameObject.FindGameObjectWithTag("MainCamera").transform);
                hand.transform.localPosition = new Vector3(0, -3.5f, 8);
            }
            else
            {
                hand.transform.localPosition = new Vector3(0, 13.5f, -0.5f);
                hand.transform.SetParent(table.transform);
            }
        }

        


    }

   

    public void DealCardToPlayer(DraggableEntity draggableEntity)
    {
        hand.DropCardOnClients(draggableEntity);
    }

    [TargetRpc]
    void TargetSpawnHand(NetworkConnection target)
    {
        this.transform.GetChild(0).localPosition = new Vector3(0, -2.7f, 0);
    }


    



    






}
