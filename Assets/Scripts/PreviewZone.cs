﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PreviewZone : MonoBehaviour
{
    public static PreviewZone Instance { get; private set; }

    public PreviewZoneCard previewZoneCardPrefab;


    void Awake()
    {
        if (Instance == null) Instance = this;
        this.gameObject.SetActive(false);
    }

    public void OnChildBeginDrag(PreviewZoneCard child)
    {
        this.gameObject.SetActive(false);
    }

    public void Init(List<CardView> cardViewsToAdd)
    {
        for (int i = 1; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }


            this.gameObject.SetActive(true);
        Vector3 startingPosition = new Vector3(GetComponent<SpriteRenderer>().sprite.bounds.size.x / 2, 0, 0);

        for (int i = 0; i < cardViewsToAdd.Count; i++)
        {
            PreviewZoneCard previewZoneCardToSpawn = Instantiate(previewZoneCardPrefab, this.transform);
            var newPos = -startingPosition + new Vector3(1.5f + (i) * 3f, 0, 0);
            previewZoneCardToSpawn.transform.position = newPos;
            previewZoneCardToSpawn.SetCardView(cardViewsToAdd[i]);
        }
    }

}
