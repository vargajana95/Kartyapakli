﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(BoxCollider2D), typeof(SpriteRenderer))]
public class PreviewZoneCard : MonoBehaviour, IBeginDragHandler, IDragHandler
{
    private CardView cardView;

    public void OnBeginDrag(PointerEventData eventData)
    {
        this.transform.parent.GetComponent<PreviewZone>().OnChildBeginDrag(this);

        cardView.GetComponent<BoxCollider2D>().enabled = false;
        eventData.pointerDrag = cardView.gameObject;
    }



    public void OnDrag(PointerEventData eventData)
    {
    }

    public void SetCardView(CardView cardView)
    {
        this.cardView = cardView;
        GetComponent<SpriteRenderer>().sprite = cardView.GetComponent<SpriteRenderer>().sprite;
    }


}
