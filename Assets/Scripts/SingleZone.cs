﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

public class SingleZone : Zone {

    private PreviewZone previewZone;

    [SyncVar]
    public Permission.AllowType viewPermissionType;

    public Permission ViewPermission { get; set; }

    protected override void Start()
    {
        base.Start();
        previewZone = PreviewZone.Instance;

        //viewPermission = new Permission(Permission.AllowType.WARNING, Permission.AccessType.OWNERONLY);
        Debug.Log(viewPermissionType);

        ViewPermission = new Permission(viewPermissionType);
    }

    public override void OnItemDropped(DraggableEntity entity)
    {
        base.OnItemDropped(entity);

        //MyNetwork.Instance.DropCardTo(this.gameObject, entity.gameObject, entity.transform.GetSiblingIndex());

        //TODO dont pass CardView, pass DraggableEntity instead
        //CommandProcessor.Instance.ExecuteClientCommand(new CommandDropCardTo(MyNetwork.Instance.Player, this, entity.GetComponent<CardView>(), entity.transform.GetSiblingIndex()));


        //entity.GetComponent<CardView>().SetFacingUp(true);
        /*if (!entity.GetComponent<CardView>().isFaceingUp)
            CommandProcessor.Instance.ExecuteClientCommand(new CommandSetFacingUp(MyNetwork.Instance.Player, entity.GetComponent<CardView>(), true));*/

        /*CommandProcessor.Instance.ExecuteClientCommand(new MultiCommand(
            new CommandDropCardTo(MyNetwork.Instance.Player, this, entity.GetComponent<CardView>(), entity.transform.GetSiblingIndex()),
            new CommandSetFacingUp(MyNetwork.Instance.Player, entity.GetComponent<CardView>(), true)));*/

        entity.GetComponent<CardView>().SetFacingUp(true);
        CommandProcessor.Instance.ExecuteClientCommand((new CommandDropCardTo(MyNetwork.Instance.Player, this, entity.GetComponent<CardView>(), entity.transform.GetSiblingIndex())));


        PlaceDraggableEntity(entity);

        MoveCardsInside();


        entity.DroppedOnZone(this);
    }


    public override void OnItemEndDrag(DraggableEntity draggedItem)
    {
        //draggedItem.GetComponent<BoxCollider2D>().enabled = false;

        //TODO move it to onDrop
        //draggedItem.GetComponent<CardView>().SetFacingUp(true);
    }

    public override void PlaceDraggableEntity(DraggableEntity entity)
    {
        entity.transform.SetParent(this.transform);
        entity.transform.DOLocalMove(Vector3.zero, 0.2f);
        entity.GetComponent<CardView>().SetFacingUp(true);
        UpdateCardsSortingOrder();
    }

    public override void OnTouch()
    {

        ViewPermission.Check(MyNetwork.Instance.Player, ownerNetId,
            ShowPreview, () => { },
            "Are you sure?");
            

    }


    private void ShowPreview()
    {
        List<CardView> cardViews = new List<CardView>();

        for (int i = 1; i < transform.childCount; i++)
        {
            cardViews.Add(transform.GetChild(i).GetComponent<CardView>());
        }

        previewZone.Init(cardViews);
    }

    
}
