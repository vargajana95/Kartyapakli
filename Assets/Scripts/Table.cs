﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

public class Table : NetworkBehaviour, IDropHandler {

    public void OnDrop(PointerEventData eventData)
    {
        DraggableEntity draggableEntity = eventData.pointerDrag.GetComponent<DraggableEntity>();

        //Return it back
        draggableEntity.ReturnToLastZone();
    }

    // Use this for initialization
    void Start () {
        BoxCollider2D collider= GetComponent<BoxCollider2D>();

        float cameraHeight = Camera.main.orthographicSize * 2;
        Vector2 cameraSize = new Vector2(Camera.main.aspect * cameraHeight, cameraHeight);

        collider.size = cameraSize;
    }
	
}
