﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UndoRedoUI : MonoBehaviour {
    [SerializeField]
    private Button undoButton;

    [SerializeField]
    private Button redoButton;

    public void OnUndoButtonClicked()
    {
        CommandProcessor.Instance.CmdUndoLastServerCommand();
    }

    public void OnRedoButtonClicked()
    {
        CommandProcessor.Instance.CmdRedoLastServerCommand();
    }


}
