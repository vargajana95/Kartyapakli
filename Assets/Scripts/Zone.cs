﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;

public abstract class Zone : Entity, IDropZone, IHasContextMenu
{

    [SyncVar(hook = "OnOwnerChanged")]
    public NetworkInstanceId ownerNetId;

    [SyncVar]
    public Permission.AllowType takeAwayPermissionType;
    [SyncVar]
    public Permission.AllowType dropOntoPermissionType;
    [SyncVar]
    public string zoneName;
    [SyncVar]
    public Color zoneColor = Color.white;


    public Permission TakeAwayPermission { get; set; }
    public Permission DropOntoPermission { get; set; }
    public Permission SetOwnerPermission { get; set; }

    private ContextMenu contextMenu;

    protected GameObject mainCamera;



    protected override void Start()
    {
        base.Start();

        /*takeAwayPermission = new Permission(Permission.AllowType.WARNING, Permission.AccessType.OWNERONLY);
        dropOntoPermission = new Permission(Permission.AllowType.WARNING, Permission.AccessType.OWNERONLY);*/
        TakeAwayPermission = new Permission(takeAwayPermissionType);
        DropOntoPermission = new Permission(dropOntoPermissionType);
        SetOwnerPermission = new Permission(Permission.AllowType.VOTE);

        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");

        InitContextMenu();
    }                                               
                                                    
    public void OnDrop(PointerEventData eventData)
    {                                               
        /*DropZone parentZone = eventData.pointerDrag.transform.parent.GetComponent<DropZone>();
        if (parentZone != null)                     
        {                                           
            parentZone.OnCardRemoved();             
        }*/                                         
        DraggableEntity draggableEntity = eventData.pointerDrag.GetComponent<DraggableEntity>();

        DropOntoPermission.Check(MyNetwork.Instance.Player, ownerNetId, 
            () => { OnItemDropped(draggableEntity); },
            () => { draggableEntity.lastParentZone.DropCardOnClients(draggableEntity, draggableEntity.GetComponent<CardView>().ExSiblingIndex); }, "Are you sure?");                                                                 
    }        
    
    public virtual void OnItemDropped(DraggableEntity entity)
    {
        entity.transform.SetParent(this.transform);

        entity.GetComponent<CardView>().SetLocallyFacingUp(false);


        IDropZone parentZone = entity.lastParentZone;
        if (parentZone != null && parentZone != (IDropZone)this)
        {

            parentZone.OnCardRemoved();
        }
        entity.OnDrop();
    }
                                                    
    public abstract void PlaceDraggableEntity(DraggableEntity entity);
                                                    
                                                    
    public virtual void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
        {
            eventData.pointerDrag.GetComponent<SpriteRenderer>().sortingOrder = transform.childCount;
            eventData.pointerDrag.GetComponent<DraggableEntity>().FutureParent = this.transform;
        }
    }

    public virtual void OnPointerExit(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
            eventData.pointerDrag.GetComponent<DraggableEntity>().FutureParent = null;
    }


    public virtual void DropCardOnClients(DraggableEntity card, int siblingIndex = -1)
    {
        //IDropZone parentZone = card.transform.parent.GetComponent<IDropZone>();
        IDropZone parentZone = card.lastParentZone;

        card.transform.SetParent(this.transform);
        if (siblingIndex != -1)
            card.transform.SetSiblingIndex(siblingIndex);
        //card.lastPosition = Vector3.zero;

        card.GetComponent<CardView>().SetLocallyFacingUp(false);

        PlaceDraggableEntity(card);
        UpdateCardsSortingOrder();
        MoveCardsInside();


        card.DroppedOnZone(this);

        if (parentZone != null && parentZone != (IDropZone) this)
        {

            parentZone.OnCardRemoved();
        }
    }

    public virtual void OnCardRemoved()
    {
        MoveCardsInside();
    }

    public virtual void OnItemBeginDrag(DraggableEntity draggedItem) {
        if (draggedItem.IsDraggable)
            return;

        TakeAwayPermission.Check(MyNetwork.Instance.Player, ownerNetId,
            () => { draggedItem.IsDraggable = true; },
            () => { draggedItem.IsDraggable = false; }, "Are you sure?");

        draggedItem.transform.parent = mainCamera.transform;

    }
    public virtual void OnItemDrag(DraggableEntity draggedItem) { }
    public virtual void OnItemEndDrag(DraggableEntity draggedItem) {}

    void OnOwnerChanged(NetworkInstanceId ownerId)
    {
        this.ownerNetId = ownerId;
    }

    public override void  OnLongTouch()
    {
        base.OnLongTouch();
        if (contextMenu != null)
            ContextMenuPanel.Instance().Show(Input.mousePosition, contextMenu);
    }

    public virtual void InitContextMenu()
    {
        contextMenu = new ContextMenu();
        contextMenu.AddMenuItem(new ContextMenu.ContextMenuItem("Set owner", () => {
            //MyNetwork.Instance.SetZoneOwner(this.netId, MyNetwork.Instance.Player.netId);
            SetOwnerPermission.Check(MyNetwork.Instance.Player, ownerNetId,
                () =>
                {
                    CommandProcessor.Instance.ExecuteClientCommand(new CommandSetZoneOwner(this, MyNetwork.Instance.Player));
                    //MyNetwork.Instance.SetZoneOwner(this.netId, MyNetwork.Instance.Player.netId);
                    Debug.Log("Owner set");
                },
                () => { Debug.Log("Vote Not Passsed"); },
                "Set owner?");
            /*VoteManager.Instance.CreateVote("Set owner?",
                () => { MyNetwork.Instance.SetZoneOwner(this.netId, MyNetwork.Instance.Player.netId); Debug.Log("Owner set"); },
                () => { Debug.Log("Vote Not Passsed"); });*/
            
        }));
        //contextMenu.AddMenuItem(new ContextMenu.ContextMenuItem("Lol", () => {}));
    }

    public virtual ContextMenu GetContextMenu()
    {
        return contextMenu;
    }

    public virtual void UpdateCardsSortingOrder()
    {
        foreach (Transform child in transform)
        {
            child.GetComponent<SpriteRenderer>().sortingOrder = child.GetSiblingIndex();
        }
    }

    protected void MoveCardsInside()
    {
        for (int i = 1; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            child.GetComponent<DraggableEntity>().DroppedOnZone(this);
        }
    }
}
