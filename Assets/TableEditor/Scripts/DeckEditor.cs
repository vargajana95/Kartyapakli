﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckEditor : EditorEntity
{
    private static int zoneNumber = 0;

    public Permission.AllowType TakeAwayPermissionType { get; set; }

    private DeckInfo deckInfo;
    private DeckData deckData;

    public DeckInfo DeckInfo
    {
        get
        {
            return deckInfo;
        }
        set
        {
            deckInfo = value;
        }
    }

    public DeckData DeckData
    {
        get
        {
            return deckData;
        }
        set
        {
            deckData = value;
        }
    }

    private string deckDataFileName = "uno.json";
    /*public string DeckDataFileName {
        get
        {
            return deckDataFileName;
        }
        set
        {
            deckDataFileName = value;
            deckData = DeckSettings.LoadDeckDataFromFile(deckDataFileName);
            deckInfo = DeckSettings.LoadDeckInfoFromFile(deckData.deckInfoPath);
        }
    }*/

    public override void Start()
    {
        base.Start();

        Name = "Deck" + (++zoneNumber);

        deckData = DeckSettings.LoadDefaultDeckDataFromFile(deckDataFileName);
        deckInfo = DeckSettings.LoadDeckInfoFromFile(deckData.deckInfoPath);
    }


    public override void InitContextMenu()
    {
        base.InitContextMenu();
        GetContextMenu().AddMenuItem(new ContextMenu.ContextMenuItem("Zone settings", () => {
            //DeckSettings.Instance.Show(this);
            DeckPermissionSettings.Show(this);
        }));
        GetContextMenu().AddMenuItem(new ContextMenu.ContextMenuItem("Card settings", () => {
            //DeckSettings.Instance.Show(this);
            DeckSettings.Show(this);
                }));
        GetContextMenu().AddMenuItem(new ContextMenu.ContextMenuItem("Remove", () => {
            //DeckSettings.Instance.Show(this);
            Destroy(this.gameObject);
        }));

    }

    public override void Save(TableData tableData)
    {
        tableData.Add(new DeckEntityData(this.transform.position, Name, Color, deckInfo, deckData, TakeAwayPermissionType));

    }
}
