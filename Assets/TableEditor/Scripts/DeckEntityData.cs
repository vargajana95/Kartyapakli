﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

[Serializable]
public class DeckEntityData : EntityData
{
    [JsonProperty]
    private DeckData deckData;
    [JsonProperty]
    private DeckInfo deckInfo;
    [JsonProperty]
    private Permission.AllowType takeAwayPermissionType;

    /*//[SerializeField]
    [JsonProperty]
    private string deckDataFileName;*/

    [JsonConstructor]
    public DeckEntityData()
    {
        //this.deckDataFileName = deckDataFileName;
        /*deckData = DeckSettings.LoadDeckDataFromFile(deckDataFileName);
        deckInfo = DeckSettings.LoadDeckInfoFromFile(deckData.deckInfoPath);*/
    }

    public DeckEntityData(Vector3 position, string name, Color color, DeckInfo deckInfo, DeckData deckData, Permission.AllowType takeAwayPermissionType) 
        : base(position, name, color)
    {
        this.deckData = deckData;
        this.deckInfo = deckInfo;
        this.takeAwayPermissionType = takeAwayPermissionType;

        //this.deckDataFileName = deckDataFileName;
    }


    protected override Zone GetEntityToSpawn(Transform parent)
    {
        var deckEntity = UnityEngine.Object.Instantiate(TableEditorDataHolder.Instance.deckPrefab, parent);

        deckEntity.DeckData = deckData;
        deckEntity.DeckInfo = deckInfo;
        deckEntity.takeAwayPermissionType = takeAwayPermissionType;
        return deckEntity;
    }


    protected override EditorEntity GetEditorEntityToSpawn(Transform parent)
    {
        var entityToSpawn = UnityEngine.Object.Instantiate(TableEditorDataHolder.Instance.deckEditorPrefab, parent);

        //entityToSpawn.DeckDataFileName = deckDataFileName;

        entityToSpawn.DeckData = deckData;
        entityToSpawn.DeckInfo = deckInfo;

        entityToSpawn.TakeAwayPermissionType = takeAwayPermissionType;

        return entityToSpawn;
    }
}
