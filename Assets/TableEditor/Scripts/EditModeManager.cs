﻿using Prototype.NetworkLobby;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

//TODO make this MonoBeaviour?
public class EditModeManager : NetworkBehaviour {
    public GameObject editModeManagerCanvas;
    public TablesHolder tablesHolder;

    public static EditModeManager Instance { get; set; }



    void Awake () {
        Instance = this;
	}

    private void OnDestroy()
    {
        Instance = null;
    }


    public void OnBackButtonPressed()
    {
        LobbyManager.s_Singleton.ShowLobby();
        HideEditMode();
    }

    public void ShowEditMode()
    {
        editModeManagerCanvas.gameObject.SetActive(true);
    }

    public void HideEditMode()
    {
        editModeManagerCanvas.gameObject.SetActive(false);
    }

    public void SpawnEditorEntity(EditorEntity entity)
    {
        var spawnedEntity = Instantiate(entity, tablesHolder.ActiveTable.transform);
        //spawnedEntity.gameObject.SetActive(true);
        spawnedEntity.transform.position = new Vector3(0, 0, -0.5f);
    }

    /*public void Save()
    {
        tablesHolder.Save();
    }*/




}
