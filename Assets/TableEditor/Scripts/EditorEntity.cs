﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class EditorEntity : MonoBehaviour, IHasContextMenu, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerUpHandler, IPointerDownHandler
{
    private static float longTouchTime = 0.7f;

    private ContextMenu contextMenu;

    private BoxCollider2D boxCollider;
    private Vector3 pointerOffset;

    private bool isPointerDown = false;
    private bool longPressTriggered = false;
    private float timePressStarted;
    private Vector3 clickPosition;

    public string Name { get; set; }

    private Color color = Color.magenta;
    public Color Color
    {
        get
        {
            return color;
        }
        set
        {
            color = value;
        }
    }

    private void Update()
    {
        if (isPointerDown && !longPressTriggered)
        {
            if (Time.time - timePressStarted > longTouchTime)
            {
                longPressTriggered = true;
                OnLongTouch();
            }
        }
    }




    public void OnBeginDrag(PointerEventData eventData)
    {


        boxCollider.enabled = false;

        pointerOffset = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        pointerOffset.z = -this.transform.localPosition.z;
    }

    public void OnDrag(PointerEventData eventData)
    {

        var screenMousePos = Input.mousePosition;
        screenMousePos.z = 10.0f;
        transform.position = Camera.main.ScreenToWorldPoint(screenMousePos) - pointerOffset;
    }

    public abstract void Save(TableData tableData);

    public void OnEndDrag(PointerEventData eventData)
    {
        boxCollider.enabled = true;
        pointerOffset = Vector3.zero;

    }


    // Use this for initialization
    public virtual void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
        InitContextMenu();
    }

    public virtual void InitContextMenu()
    {
        contextMenu = new ContextMenu();
    }
    public virtual ContextMenu GetContextMenu()
    {
        return contextMenu;
    }


    public void OnPointerDown(PointerEventData eventData)
    {
        timePressStarted = Time.time;
        isPointerDown = true;
        longPressTriggered = false;
        clickPosition = eventData.position;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (isPointerDown && !longPressTriggered)
        {
            if (Time.time - timePressStarted <= longTouchTime)
            {
                OnTouch();
            }
        }
        isPointerDown = false;
    }


    public void OnPointerExit(PointerEventData eventData)
    {
        isPointerDown = false;
    }


    public virtual void OnTouch()
    {
        Debug.Log("Touch");
    }

    public virtual void OnLongTouch()
    {
        if (contextMenu != null)
            ContextMenuPanel.Instance().Show(Input.mousePosition, contextMenu);
    }


}
