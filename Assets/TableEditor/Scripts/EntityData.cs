﻿using Newtonsoft.Json;
using System;
using UnityEngine;
using UnityEngine.Networking;

[Serializable]
public abstract class EntityData {


    [JsonProperty]
    public Vector3 Position { get; set; }

    [JsonProperty]
    public string Name { get; set; }
    [JsonProperty]
    public string Color { get; set; }

    public EntityData()
    {
        
    }

    public EntityData(Vector3 position, string name, Color color)
    {
        Position = position;
        Name = name;


        Color = '#' + ColorUtility.ToHtmlStringRGBA(color);
    }

    public void SpawnEntity(GameObject parent)
    {
        Zone entity = GetEntityToSpawn(parent.transform);

        entity.localPosition = Position;
        entity.transform.localPosition = Position;


        entity.parent = parent;
        entity.zoneName = Name;

        Color newCol;
        if (ColorUtility.TryParseHtmlString(Color, out newCol))
            entity.zoneColor = newCol;

        NetworkServer.Spawn(entity.gameObject);
    }

    protected abstract Zone GetEntityToSpawn(Transform parent);
    protected abstract EditorEntity GetEditorEntityToSpawn(Transform parent);


    public void SpawnEditorEntity(GameObject parent)
    {
        var entityToSpawn = GetEditorEntityToSpawn(parent.transform);
        entityToSpawn.transform.localPosition = Position;
        entityToSpawn.Name = Name;
        Color newCol;
        if (ColorUtility.TryParseHtmlString(Color, out newCol))
            entityToSpawn.Color = newCol;

    }

}
