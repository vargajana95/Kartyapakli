﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalZoneEditor : SingleZoneEditor, IHasContextMenu
{

    public override void Save(TableData tableData)
    {
        tableData.Add(new HorizontalZoneData(this.transform.position, Name, Color, TakeAwayPermissionType, DropOntoPermissionType, ViewPermissionType));
    }

}
