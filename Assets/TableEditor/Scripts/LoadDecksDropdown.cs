﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadDecksDropdown : MonoBehaviour {

    private Dropdown dropDown;
    private DeckSettings.Decks decks;

    // Use this for initialization
    void Start()
    {
        dropDown = GetComponent<Dropdown>();
    }

    public void Init(DeckSettings.Decks decks)
    {
        dropDown = GetComponent<Dropdown>();
        this.decks = decks;
        LoadDecks();
    }

    private void LoadDecks()
    {
        foreach (var deck in decks.decks)
        {
            dropDown.options.Add(new Dropdown.OptionData(deck.deckName));

            dropDown.RefreshShownValue();
            //Debug.Log("name  " + name);
        }

    }

    public DeckSettings.DeckFilePath GetSelectedDeck()
    {
        return decks.decks[dropDown.value];
    }
}
