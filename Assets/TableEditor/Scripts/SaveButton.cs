﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveButton : MonoBehaviour {

    public TablesHolder tablesHolder;
    public InputField fileNameField;
    [SerializeField]
    public static InputField field;

    public void SaveToJson()
    {
        string fileName = fileNameField.text;
        if (!string.IsNullOrEmpty(fileName))
        {
            tablesHolder.SaveToJson(fileName);
        }
    }


}
