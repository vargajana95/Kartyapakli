﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[Serializable]
public class SingleZoneData : EntityData {
    [JsonProperty]
    private Permission.AllowType takeAwayPermissionType;
    [JsonProperty]
    private Permission.AllowType dropOntoPermissionType;
    [JsonProperty]
    private Permission.AllowType viewPermissionType;

    public SingleZoneData()
    {
    }

    public SingleZoneData(Vector3 position, string name, Color color, Permission.AllowType takeAwayPermissionType, Permission.AllowType dropOntoPermissionType, Permission.AllowType viewPermissionType) 
        : base(position, name, color)
    {

        this.takeAwayPermissionType = takeAwayPermissionType;
        this.dropOntoPermissionType = dropOntoPermissionType;
        this.viewPermissionType = viewPermissionType;
    }

    protected override Zone GetEntityToSpawn(Transform parent)
    {
        var entity = UnityEngine.Object.Instantiate(TableEditorDataHolder.Instance.singleZonePrefab, parent);

        entity.takeAwayPermissionType = takeAwayPermissionType;
        entity.dropOntoPermissionType = dropOntoPermissionType;
        entity.viewPermissionType = viewPermissionType;

        return entity;
    }

    protected override EditorEntity GetEditorEntityToSpawn(Transform parent)
    {
        var entityToSpawn = UnityEngine.Object.Instantiate(TableEditorDataHolder.Instance.singleZoneEditorPrefab, parent);

        entityToSpawn.TakeAwayPermissionType = takeAwayPermissionType;
        entityToSpawn.DropOntoPermissionType = dropOntoPermissionType;
        entityToSpawn.ViewPermissionType = viewPermissionType;

        return entityToSpawn;
    }


}
