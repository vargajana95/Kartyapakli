﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleZoneEditor : EditorEntity, IHasContextMenu
{
    private static int zoneNumber = 0;


    public Permission.AllowType TakeAwayPermissionType { get; set; }
    public Permission.AllowType DropOntoPermissionType { get; set; }
    public Permission.AllowType ViewPermissionType { get; set; }

    public override void Start()
    {
        base.Start();

        Name = "Zone"+ (++zoneNumber);
    }

    public override void InitContextMenu()
    {
        base.InitContextMenu();
        GetContextMenu().AddMenuItem(new ContextMenu.ContextMenuItem("Settings", () => {
            //DeckSettings.Instance.Show(this);
            SingleZoneSettings.Show(this);
        }));
        GetContextMenu().AddMenuItem(new ContextMenu.ContextMenuItem("Remove", () => {
            //DeckSettings.Instance.Show(this);
            Destroy(this.gameObject);
        }));
    }

    public override void Save(TableData tableData)
    {
        tableData.Add(new SingleZoneData(this.transform.position, Name, Color, TakeAwayPermissionType, DropOntoPermissionType, ViewPermissionType));
    }

}
