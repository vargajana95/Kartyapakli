﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnButton : MonoBehaviour {
    public EditorEntity prefab;

	// Use this for initialization
	void Start () {
        this.gameObject.GetComponent<Button>().onClick.AddListener( () => { EditModeManager.Instance.SpawnEditorEntity(prefab); });
	}
	
}
