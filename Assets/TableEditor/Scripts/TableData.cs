﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[Serializable]
public class TableData {
    //[SerializeField]
    [JsonProperty]
    private List<EntityData> entities = new List<EntityData>();


    public void Add(EntityData editorEntity)
    {
        entities.Add(editorEntity);
    }

    internal void Clear()
    {
        entities.Clear();
    }

    public void SpawnEntities(GameObject parent)
    {
        foreach (var entity in entities)
        {
            entity.SpawnEntity(parent);
        }
    }

    public void SpawnEditorEntities(GameObject parent)
    {
        foreach (var entity in entities)
        {
            entity.SpawnEditorEntity(parent);
        }
    }
}
