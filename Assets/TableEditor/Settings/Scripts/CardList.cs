﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardList : MonoBehaviour
{
    public CardListItem cardListItemPrefab;

    private DeckData deckData;


    public void Load(DeckInfo deckInfo, DeckData deckData)
    {
        this.deckData = deckData;

        DestroyChildren();
        Sprite[] sprites = Resources.LoadAll<Sprite>(deckInfo.deckImagePath);

        foreach (var card in deckInfo.cards)
        {
            CardListItem cardListItem = Instantiate(cardListItemPrefab, this.transform);
            cardListItem.Sprite = sprites[card];
            if (deckData.cardNums.Count == deckInfo.cards.Count)
                cardListItem.NumberOfCards = deckData.cardNums[card];
            else
                cardListItem.NumberOfCards = 1;
        }
    }

    public DeckData getNewDeckData()
    {
        DeckData newDeckData = new DeckData
        {
            deckInfoPath = deckData.deckInfoPath
        };

        for (int i = 0; i < transform.childCount; i++)
        {
            CardListItem item = transform.GetChild(i).GetComponent<CardListItem>();
            newDeckData.cardNums.Add(item.NumberOfCards);
        }
        return newDeckData;
    }
    private void DestroyChildren()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }
}
