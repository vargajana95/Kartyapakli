﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DeckInfo {

    public List<int> cards;

    public string deckImagePath;

    public string defaultDeckDataFileName;

    public string deckBackPath;
}
