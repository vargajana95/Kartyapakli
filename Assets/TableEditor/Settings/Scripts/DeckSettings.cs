﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class DeckSettings : Menu<DeckSettings> {
    [SerializeField]
    private LoadDropDown deckDataDp;

    [SerializeField]
    private LoadDecksDropdown deckInfoDp;

    [SerializeField]
    private Button cancelBtn;

    [SerializeField]
    private Button okBtn;

    [SerializeField]
    private Button saveBtn;

    [SerializeField]
    private InputField filenameIf;

    [SerializeField]
    private DeckInfo deckInfo;

    [SerializeField]
    private TextAsset decksFile;

    public CardList cardList;

    private DeckEditor deck;

    

    void Start()
    {
        /*Decks lol = new Decks();
        List<DeckFilePath> df = new List<DeckFilePath>();
        lol.decks.Add(new DeckFilePath { deckName = "Francia", fileName = "francia.json" });
        lol.decks.Add(new DeckFilePath { deckName = "Uno", fileName = "unoDeck.json" });
        
        Debug.Log(JsonUtility.ToJson(lol));*/

        deckDataDp.Init(Path.Combine(Application.persistentDataPath, "Decks"));
        deckInfoDp.Init(JsonUtility.FromJson<Decks>(decksFile.text));

    }

    public static DeckInfo LoadDeckInfoFromFile(string fileName)
    {
        string dataAsJson = null;
        string filePath = Path.Combine(Application.streamingAssetsPath, Path.Combine("Decks", fileName));
        #if UNITY_EDITOR || UNITY_IOS || UNITY_STANDALONE_WIN
        dataAsJson = File.ReadAllText(filePath);

        #elif UNITY_ANDROID
        WWW reader = new WWW (filePath);
        while (!reader.isDone) {
        }
        dataAsJson = reader.text;
        #endif

        return JsonUtility.FromJson<DeckInfo>(dataAsJson);
    }

    public void OnLoadDeckInfoButtonPressed()
    {
        deckInfo = LoadDeckInfoFromFile(deckInfoDp.GetSelectedDeck().fileName);
        DeckData deckData = LoadDefaultDeckDataFromFile(deckInfo.defaultDeckDataFileName);

        cardList.Load(deckInfo, deckData);
    }

    public void OnLoadDeckDataButtonPressed()
    {
        DeckData deckData = LoadDeckDataFromFile(deckDataDp.GetSelectedFileName());

        deckInfo = LoadDeckInfoFromFile(deckData.deckInfoPath);

        cardList.Load(deckInfo, deckData);
    }

    public static DeckData LoadDeckDataFromFile(string fileName)
    {
        string dataAsJson = File.ReadAllText(Path.Combine(Application.persistentDataPath, Path.Combine("Decks", fileName)));

        return JsonUtility.FromJson<DeckData>(dataAsJson);
    }

    public static DeckData LoadDefaultDeckDataFromFile(string fileName)
    {
        string dataAsJson = null;

        string filePath = Path.Combine(Application.streamingAssetsPath, Path.Combine("DeckData", fileName));
        #if UNITY_EDITOR || UNITY_IOS || UNITY_STANDALONE_WIN
        dataAsJson = File.ReadAllText(filePath);

        #elif UNITY_ANDROID
        WWW reader = new WWW (filePath);
        while (!reader.isDone) {
        }
        dataAsJson = reader.text;
        #endif
        return JsonUtility.FromJson<DeckData>(dataAsJson);
    }

    public static void Show(DeckEditor deck)
    {
        Open(new DeckSettingsCreator());
        Instance.deck = deck;
        Instance.deckInfo = deck.DeckInfo;
        //Instance.gameObject.SetActive(true);

        Instance.cardList.Load(deck.DeckInfo, deck.DeckData);
        //MenuManager.Instance.CreateInstance(new DeckSettingsCreator());
    }
    public static void Hide()
    {
        Close();
    }

    public void OnCancelPressed()
    {
        Hide();
    }


    public void OnOkButtonPressed()
    {
        deck.DeckData = cardList.getNewDeckData();
        deck.DeckInfo = deckInfo;
        Hide();
    }

    public void OnSaveButtonPressed()
    {
        string fileName = filenameIf.text;
        if (!String.IsNullOrEmpty(fileName))
        {
            DeckData deckData = cardList.getNewDeckData();
            
            SaveDeckDataToFile(fileName, deckData);
        }
    }

    public static void SaveDeckDataToFile(string fileName, DeckData deckData)
    {
        DirectoryInfo di = new DirectoryInfo(Path.Combine(Application.persistentDataPath, "Decks"));
        if (!di.Exists)
            di.Create();

        Debug.Log(Path.Combine(Application.persistentDataPath, Path.Combine("Decks", fileName)));
        var sr = File.CreateText(Path.Combine(Application.persistentDataPath, Path.Combine("Decks", fileName)));
        sr.WriteLine(JsonUtility.ToJson(deckData));

        sr.Close();
    }

    public class DeckSettingsCreator : MenuManager.IMenuCreator
    {
        public void InstanciateMenu(Transform parent)
        {
            Instantiate(MenuManager.Instance.deckSettingsPrefab, parent);
        }
    }

    [Serializable]
    public class  Decks
    {
        public List<DeckFilePath> decks = new List<DeckFilePath>();
    }

    [Serializable]
    public class DeckFilePath
    {
        public string deckName;
        public string fileName;

        public DeckFilePath() { }
    }


}
