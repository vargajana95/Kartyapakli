
# Kártyapakli

A LAN multiplayer, cross-platform card game application, made with Unity. The application enables a wide range of customizations, including:
* Setting the invidual number of cards in a deck
* Creating custom layout for different games
* Adding and moving different types of zones
* Cusomizing the different rules for each game

![In game](Images/Capture6.PNG)

![Layout editor](Images/Capture5.PNG)

![Lobby screen](Images/Capture2.PNG)

![Deck customization screen](Images/Capture.PNG)

![Rule customization screen](Images/Capture3.PNG)

![Multiple cards in one zone](Images/Capture4.PNG)

